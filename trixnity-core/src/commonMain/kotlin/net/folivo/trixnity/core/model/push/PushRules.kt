package net.folivo.trixnity.core.model.push

typealias PushRules = Map<PushRuleKind, List<PushRule>?>
