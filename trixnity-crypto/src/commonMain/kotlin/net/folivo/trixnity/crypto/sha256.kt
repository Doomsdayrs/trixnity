package net.folivo.trixnity.crypto

expect suspend fun sha256(input: ByteArray): String