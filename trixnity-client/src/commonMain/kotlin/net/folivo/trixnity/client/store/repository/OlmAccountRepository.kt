package net.folivo.trixnity.client.store.repository

interface OlmAccountRepository : MinimalStoreRepository<Long, String>