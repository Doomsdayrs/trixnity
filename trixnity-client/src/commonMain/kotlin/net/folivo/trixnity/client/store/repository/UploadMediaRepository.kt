package net.folivo.trixnity.client.store.repository

import net.folivo.trixnity.client.store.UploadCache

interface UploadMediaRepository : MinimalStoreRepository<String, UploadCache>