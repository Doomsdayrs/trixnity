package net.folivo.trixnity.client.store.repository

import net.folivo.trixnity.client.store.Account

interface AccountRepository : MinimalStoreRepository<Long, Account>